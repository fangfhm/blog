

#配置java，参考java配置

/blog/基础环境/linux添加java环境变量.md

###### #添加用户

```shell
useradd els
```

###### #环境变量

```shell
vi /etc/security/limits.conf 
els          soft    nofile         1024000
els          hard    nofile         1024000
els          soft    memlock        unlimited
els          hard    memlock        unlimited

echo "vm.max_map_count=262144" >> /etc/sysctl.conf 
```

###### # elasticsearch 配置

```shell
cp /tmp/elasticsearch-6.5.4.tar.gz /home/els/
chown els:els /home/els/elasticsearch-6.5.4.tar.gz
tar -zxvf  elasticsearch-6.5.4.tar.gz


vi /home/els/elasticsearch-6.5.4/config/elasticsearch.yml
http.cors.allow-headers: Authorization,X-Requested-With,Content-Length,Content-Type
http.cors.enabled: true
http.cors.allow-origin: /.*/
xpack.security.enabled: true
xpack.security.transport.ssl.enabled: true

nohup /home/els/elasticsearch-6.5.4/bin/elasticsearch >/dev/null &
```

###### #添加license

```shell
vi license.json
{"license":{"uid":"72ee62fb-865a-4887-9c87-168fe12a1265","type":"platinum","issue_date_in_millis":1545868800000,"expiry_date_in_millis":4102329600000,"max_nodes":100,"issued_to":"jin king (ccn)","issuer":"Web Form","signature":"AAAAAwAAAA02Dgj8/hUDfzKEQ2nrAAABmC9ZN0hjZDBGYnVyRXpCOW5Bb3FjZDAxOWpSbTVoMVZwUzRxVk1PSmkxaktJRVl5MUYvUWh3bHZVUTllbXNPbzBUemtnbWpBbmlWRmRZb25KNFlBR2x0TXc2K2p1Y1VtMG1UQU9TRGZVSGRwaEJGUjE3bXd3LzRqZ05iLzRteWFNekdxRGpIYlFwYkJiNUs0U1hTVlJKNVlXekMrSlVUdFIvV0FNeWdOYnlESDc3MWhlY3hSQmdKSjJ2ZTcvYlBFOHhPQlV3ZHdDQ0tHcG5uOElCaDJ4K1hob29xSG85N0kvTWV3THhlQk9NL01VMFRjNDZpZEVXeUtUMXIyMlIveFpJUkk2WUdveEZaME9XWitGUi9WNTZVQW1FMG1DenhZU0ZmeXlZakVEMjZFT2NvOWxpZGlqVmlHNC8rWVVUYzMwRGVySHpIdURzKzFiRDl4TmM1TUp2VTBOUlJZUlAyV0ZVL2kvVk10L0NsbXNFYVZwT3NSU082dFNNa2prQ0ZsclZ4NTltbU1CVE5lR09Bck93V2J1Y3c9PQAAAQAXQpVbyDECAris6ObPj0P88cyrfD5sCQjMTLn0Jm1Yr0+qPXmqlk2F8KiOU8XoEtk0hAtTRvD9e/aFVGliOJYxF6d5YOIuG3gbMsz1/jhj5boQkczr/fhTcLDC+3myoEswo9QhZmsLqgqfAeXr/Uj0T09/TUHQCi/GfhIg7jEvthlQeMyzvYoSFIe6/gmCib01Mwb2UyaWnTkgCnE3v3ZvwEV5nW884esGOJ6dAmrqCnaqddDKa4N0CfNq49yPQVlJktAcuVViZF3eaIzA2+XhKLVvvvtdrxPvWeUNUxvJf9vbfaThbhleK9aR4Ym9QSNDwfhVww9nt21+UW2xESHw","start_date_in_millis":1545868800000}}

```

###### #无用户版本激活xpack

```shell
curl -XPUT 'http://127.0.0.1:9200/_xpack/license' -H "Content-Type: application/json" -d @license.json
```

###### #有用户版本激活xpack

```shell
#创建用户并设置密码
./elasticsearch-users useradd els
uri: 127.0.0.1:9200
user: esdev
password: 123456
cluster.name: default

# 赋权
./elasticsearch-users roles els -a superuser
# elasticsearch-head 链接
访问：http://127.0.0.1:9200/?auth_user=es&auth_password=SQidc@2016
#激活xpack
curl -XPUT -u els 'http://127.0.0.1:9200/_xpack/license' -H "Content-Type: application/json" -d @license.json


```

