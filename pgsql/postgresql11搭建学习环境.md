1. 配置建议：

4G内存，40G磁盘，2核以上，NAT网络模式。

2. 安装建议：

minimal最小化安装。

3. root密码：

记住你设置的root密码。

4. Linux安装配置建议

配置主机名，配置网络（根据你的vmware NAT网络进行配置），关闭selinux，关闭防火墙或开放ssh端口(测试环境)。



配置系统环境

```shell
vi /etc/sysctl.conf  
#追加到文件末尾   
kernel.shmall = 1048576  
kernel.shmmax = 4294967295  
kernel.shmmni = 4096  
kernel.sem = 50100 64128000 50100 1280  
fs.file-max = 7672460  
fs.aio-max-nr = 1048576  
net.ipv4.ip_local_port_range = 9000 65000  
net.core.rmem_default = 262144  
net.core.rmem_max = 4194304  
net.core.wmem_default = 262144  
net.core.wmem_max = 4194304  
net.ipv4.tcp_max_syn_backlog = 4096  
net.core.netdev_max_backlog = 10000  
net.ipv4.netfilter.ip_conntrack_max = 655360  
net.ipv4.tcp_timestamps = 0  
net.ipv4.tcp_tw_recycle = 1  
net.ipv4.tcp_timestamps = 1  
net.ipv4.tcp_keepalive_time = 72   
net.ipv4.tcp_keepalive_probes = 9   
net.ipv4.tcp_keepalive_intvl = 7  
vm.zone_reclaim_mode = 0  
vm.dirty_background_bytes = 40960000  
vm.dirty_ratio = 80  
vm.dirty_expire_centisecs = 6000  
vm.dirty_writeback_centisecs = 50  
vm.swappiness = 0  
vm.overcommit_memory = 0  
vm.overcommit_ratio = 90  
```

```
参数说明

kernel.shmall
该参数控制可以使用的共享内存的总页数。 Linux 共享内存页大小为  4KB,  共享内存段的大小都是共享内存页大小的整数倍。
一个共享内存段的最大大小是 16G ，那么需要共享内存页数是  16000000KB/4KB==4194304  （页），
835596
1048576
4294967295
kernel.shmmax
内存为 16G 时，该值为 16*1024*1024*1024-1 = 17179869183 
```

