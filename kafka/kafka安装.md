官网

https://www.apache.org/dyn/closer.cgi?path=/kafka/1.0.0/kafka_2.11-1.0.0.tgz

参看：http://kafka.apachecn.org/quickstart.html

```
tar -xzf kafka_*.tgz
cd kafka_*/
bin/zookeeper-server-start.sh config/zookeeper.properties
#现在启动Kafka服务器：
bin/kafka-server-start.sh config/server.properties

#让我们创建一个名为“test”的topic，它有一个分区和一个副本：
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
#现在我们可以运行list（列表）命令来查看这个topic：
bin/kafka-topics.sh --list --zookeeper localhost:2181
#Kafka自带一个命令行客户端，它从文件或标准输入中获取输入，并将其作为message（消息）发送到Kafka集群。默认情况下，每行将作为单独的message发送。运行 producer，然后在控制台输入一些消息以发送到服务器。
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic test
This is a message
This is another message

#启动一个 consumer Kafka 还有一个命令行consumer（消费者），将消息转储到标准输出。
bin/kafka-console-consumer.sh --bootstrap-server kafka1:9092 --topic zabbixVm --from-beginning
This is a message
This is another message

```