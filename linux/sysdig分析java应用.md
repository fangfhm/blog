背景

最近老是有java应用莫名奇妙就卡死了，gc也正常，oom也没有溢出，怀疑是线程给堆死了。



开干

使用sysdig要获取应用的3个属性：

当前线程数，线程运行时间，线程对应的哪些请求，这样我就知道他是怎么死的了。



先安装

```
1)安装资源库
rpm --import https://s3.amazonaws.com/download.draios.com/DRAIOS-GPG-KEY.public
curl -s -o /etc/yum.repos.d/draios.repo http://download.draios.com/stable/rpm/draios.repo

2)安装epel包
yum install epel-release
4)安装sysdig包
yum -y install sysdig
```

