#覆盖全输出

command > log.log

#追加全输出

command >> log.log

#只输出错误

command 2> log.log

#输出重定向

command  > log.log 2>&1



#知识点

1 、> 默认就是1>

2、2>&1 比 > 更全,增加了标准输出，举例：以下> 和 2>&1进行对比。

[root@localhost ~]# cat /tmp/hostalarm 
[root@localhost ~]# /usr/lib/zabbix/externalscripts/ipmialarm.sh > /tmp/hostalarm
SEL has no entries
SEL has no entries
[root@localhost ~]# cat /tmp/hostalarm
[root@localhost ~]# /usr/lib/zabbix/externalscripts/ipmialarm.sh > /tmp/hostalarm 2>&1
[root@localhost ~]# cat /tmp/hostalarm
SEL has no entries
SEL has no entries

3、2代表错误，1代表全量。